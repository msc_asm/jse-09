package ru.gasanov.tm.service;

import ru.gasanov.tm.repository.TaskRepository;
import ru.gasanov.tm.entity.Task;

import java.util.List;

public class TaskService {

    private TaskRepository taskRepository;

    public Task create(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name);
    }

    public void clear() {
        taskRepository.clear();
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task create(String name, String description) {
        if (description == null || description.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name, description);
    }

    public Task update(Long id, String name, String description) {
        if (description == null || description.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        if (id == null ) return null;
        return taskRepository.update(id, name, description);
    }

    public Task findByIndex(int index) {
        return taskRepository.findByIndex(index);
    }

    public Task findByName(String name) {
        return taskRepository.findByName(name);
    }

    public Task removeByName(String name) {
        return taskRepository.removeByName(name);
    }

    public Task removeById(Long id) {
        return taskRepository.removeById(id);
    }

    public Task removeByIndex(int index) {
        return taskRepository.removeByIndex(index);
    }

    public Task findById(Long id) {
        return taskRepository.findById(id);
    }
}
