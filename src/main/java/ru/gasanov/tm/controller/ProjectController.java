package ru.gasanov.tm.controller;

import ru.gasanov.tm.repository.ProjectRepository;
import ru.gasanov.tm.entity.Project;

public class ProjectController extends AbstractController{

    private final ProjectRepository projectRepository;

    public ProjectController(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public  int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("PLEASE ENTER PROJECT NAME");
        final String name = scanner.nextLine();
        projectRepository.create(name);
        System.out.println("[OK]");
        return 0;
    }

    public  int updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) -1;
        final Project project = projectRepository.findByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[PLEASE ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        System.out.println("[PLEASE ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectRepository.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final Long id= Long.parseLong(scanner.nextLine());
        final Project project = projectRepository.findById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[PLEASE ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        System.out.println("[PLEASE ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectRepository.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("[PLEASE ENTER PROJECT INDEX:");
        final int index = scanner.nextInt() - 1;
        final Project project = projectRepository.removeByIndex(index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("[PLEASE ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        final Project project = projectRepository.removeByName(name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("[PLEASE ENTER PROJECT ID:");
        final Long id = scanner.nextLong();
        final Project project = projectRepository.removeById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }


    public int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectRepository.clear();
        System.out.println("[OK]");
        return 0;
    }

    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

    public int viewProjectByIndex() {
        System.out.println("ENTER PROJECT INDEX:");
        final int index = scanner.nextInt() - 1;
        final Project project = projectRepository.findByIndex(index);
        viewProject(project);
        return 0;
    }

    public int viewProjectById() {
        System.out.println("ENTER PROJECT ID:");
        final Long id = scanner.nextLong();
        final Project project = projectRepository.findById(id);
        viewProject(project);
        return 0;
    }

    public int viewProjectByName() {
        System.out.println("ENTER PROJECT NAME:");
        final Long name = scanner.nextLong();
        final Project project = projectRepository.findById(name);
        viewProject(project);
        return 0;
    }

    public int listProject() {
        System.out.println("[LIST PROJECT]");
        System.out.println(projectRepository.findAll());
        System.out.println("[OK]");
        return 0;
    }
}
