package ru.gasanov.tm.controller;

import ru.gasanov.tm.entity.Task;

public class SystemController {

    public static void displayWelcome() {
        System.out.println("* WELCOME TO TASK MANAGER *");
    }

    public int  displayAbout() {
        System.out.println("Gasanov Asiman");
        System.out.println("gasanov_ad@nlmk.com");
        return 0;
    }

    public  int displayError() {
        System.out.println("ERROR! You passed wrong parameters ");
        return -1;
    }

    public int  displayHelp() {
        System.out.println("version - Display program version.");
        System.out.println("about - Display programm info.");
        System.out.println("help - Display list of terminal commands.");
        System.out.println("exit - Terminate console.");
        System.out.println("project-list - Display list of projects.");
        System.out.println("project-clear - Clear list of projects.");
        System.out.println("project-create - Create project.");
        System.out.println("task-list - Display list of tasks.");
        System.out.println("task-clear - Clear list of tasks.");
        System.out.println("task-create - Create tasks.");
        System.out.println("project-view-by-index - View projects by index");
        System.out.println("project-view-by-id - View projects by id");
        System.out.println("project-remove-by-id - Remove project by id");
        System.out.println("project-remove-by-name  - Remove project by name");
        System.out.println("project-remove-by-index - Remove project by index");
        System.out.println("project-update-by-index - Update project by index");
        System.out.println("project-update-by-id - Update project by id");
        System.out.println("task-create  - Create task");
        System.out.println("task-clear - Clear list of tasks");
        System.out.println("task-list - Show list of tasks");
        System.out.println("task-view-by-index - View task by index");
        System.out.println("task-view-by-id - View task by id");
        System.out.println("task-remove-by-id - Remove task by id");
        System.out.println("task-remove-by-name - Remove task by name");
        System.out.println("task-remove-by-index - Remove task by id");
        System.out.println("task-update-by-index - Update task by index");
        System.out.println("task-update-by-id - Update task by id");
        return 0;
    };

    public int displayExit() {
        System.out.println(" Terminate programm");
        return 0;
    };

    public int  displayVersion() {
        System.out.println("1.0.0");
        return 0;
    };

}
