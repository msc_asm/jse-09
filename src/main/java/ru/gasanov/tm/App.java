package ru.gasanov.tm;

import ru.gasanov.tm.controller.ProjectController;
import ru.gasanov.tm.controller.SystemController;
import ru.gasanov.tm.controller.TaskController;
import ru.gasanov.tm.repository.ProjectRepository;
import ru.gasanov.tm.repository.TaskRepository;

import java.util.Scanner;
import static ru.gasanov.tm.constant.TerminalConst.*;

public class App {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final TaskController taskController = new TaskController(taskRepository);

    private final ProjectController projectController = new ProjectController(projectRepository);

    private final SystemController systemController = new SystemController();


    {
        projectRepository.create("Demo project #1", "My first project");
        projectRepository.create("Demo project #2", "My second project");
        taskRepository.create("Demo task #1","My first task");
        taskRepository.create("Demo task #2","My second task");

    }

    public static void main(final String[] args) {
        final App app = new App();
        app.run(args);
        app.systemController.displayWelcome();
        final Scanner scanner = new Scanner(System.in);
        String command  = "";
        while (!CMD_EXIT.equals(command)){
            command = scanner.nextLine();
             app.run(command);
        }
    }

    public void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    public int run(final String param) {
        if (param == null) return -1;
        switch (param) {
            case CMD_HELP: return systemController.displayHelp();
            case CMD_ABOUT: return systemController.displayAbout();
            case CMD_VERSION: return systemController.displayVersion();
            case CMD_EXIT: return systemController.displayExit();

            case PROJECT_LIST: return projectController.listProject();
            case PROJECT_VIEW_BY_INDEX: return projectController.viewProjectByIndex();
            case PROJECT_VIEW_BY_ID: return projectController.viewProjectById();
            case PROJECT_VIEW_BY_NAME: return projectController.viewProjectByName();
            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID: return projectController.updateProjectById();
            case PROJECT_CLEAR: return projectController.clearProject();
            case PROJECT_CREATE: return projectController.createProject();

            case TASK_LIST: return taskController.listTask();
            case TASK_VIEW_BY_INDEX: return taskController.viewTaskByIndex();
            case TASK_VIEW_BY_ID: return taskController.viewTaskById();
            case TASK_REMOVE_BY_NAME: return taskController.removeTaskByName();
            case TASK_REMOVE_BY_ID: return taskController.removeTaskById();
            case TASK_REMOVE_BY_INDEX: return taskController.removeTaskByIndex();
            case TASK_UPDATE_BY_INDEX: return taskController.updateTaskByIndex();
            case TASK_UPDATE_BY_ID: return taskController.updateTaskById();
            case TASK_CLEAR: return taskController.clearTask();
            case TASK_CREATE: return taskController.createTask();

            default: return systemController.displayError();
        }
    }

}
